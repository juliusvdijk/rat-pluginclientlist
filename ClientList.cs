﻿using Server.Events;
using Server.Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Collections.ObjectModel;
using Server.Events.Client;
using Server.Plugin.DefaultInterfaces;

namespace Merccy.Plugins
{
    public class ClientList : IClientList
    {
        private ListView view;
        private ClientManager cm;
        private TaskScheduler uiScheduler;

        private ObservableCollection<Client> clientSource;
        private Dictionary<string, string> MemberKeys
        {
            get;
        }
        private Dictionary<MenuItem, Action<Client[]>> ContextMenuHandlers;

        public ClientList(ListView view, ClientManager cm)
        {
            this.view = view;
            this.cm = cm;
            uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();

            clientSource = new ObservableCollection<Client>();

            MemberKeys = new Dictionary<string, string>();
            ContextMenuHandlers = new Dictionary<MenuItem, Action<Client[]>>();

            view.ItemsSource = clientSource;

            // Register handlers on the threadpool
            cm.OnClientConnected.AddHandler((ConnectedEvent e) =>
            {
                foreach (var entry in MemberKeys)
                {
                    e.Client.Members[entry.Key] = null;
                }
            }, TaskScheduler.Default);

            // Register handlers on UI thread
            cm.OnClientConnected.AddHandler(UpdateClientList);
            cm.OnClientDisconnected.AddHandler(UpdateClientList);
        }

        public void AddHeaderProperty(string title, string binding = null)
        {
            lock (MemberKeys)
            {
                MemberKeys.Add(title, binding);
            }

            // Add header column on the UI thread
            new Task(() =>
            {
                string bindString = "Members[" + title + "]";
                if (binding != null)
                {
                    bindString += "." + binding;
                }

                GridViewColumn column = new GridViewColumn();
                column.Header = title;
                column.DisplayMemberBinding = new Binding(bindString);
                ((GridView)view.View).Columns.Add(column);
            }).Start(uiScheduler);
        }

        public void AddContextMenuItem(string title, Action<Client[]> action)
        {
            MenuItem mi = new MenuItem();
            mi.Header = title;
            mi.Click += OnMenuItemClick;

            bool exists = ContextMenuHandlers.Any(x => x.Key.Header.ToString() == title);

            lock (ContextMenuHandlers)
            {
                ContextMenuHandlers.Add(mi, action);
            }

            if (view.ContextMenu == null)
            {
                view.ContextMenu = new ContextMenu();
            }
            // Don't add items with the same name
            if (!exists)
            {
                view.ContextMenu.Items.Add(mi);
            }
        }

        // Event implementation
        private void UpdateClientList(IClientEvent e)
        {
            if (e is ConnectedEvent)
            {
                // Client connected
                clientSource.Add(e.Client);
            }
            else if (e is DisconnectedEvent)
            {
                // Client disconnected
                clientSource.Remove(e.Client);
            }
        }

        private void OnMenuItemClick(object sender, RoutedEventArgs e)
        {
            MenuItem clickedItem = (MenuItem)sender;
            foreach (var entry in ContextMenuHandlers)
            {
                // Use title to compare MenuItems
                if (entry.Key.Header.ToString() == clickedItem.Header.ToString())
                {
                    entry.Value(view.SelectedItems.OfType<Client>().ToArray());
                }
            }
        }
    }
}
