﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Server.Core;
using Server.Plugin;
using System.Windows.Markup;

namespace Merccy.Plugins
{
    [PluginManifest(Author = "Merccy", Name = "ClientList", Version = "1.0")]
    public class ClientListPlugin : PluginBase
    {
        private ClientView myView;
        public ClientListPlugin(IServerApplication serverApp) : base(serverApp)
        {
        }

        public override void OnStart()
        {
            myView = new ClientView();

            // Bind list handler to the correct view
            ClientList listHandler = new ClientList(myView.lstClients, ClientManager);

            // Add tab
            TabManager.AddOrReplaceTab("Clients", myView.gridClients);
        }
    }
}
